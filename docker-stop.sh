#!/bin/bash
echo "docker stop master/minions, if any running containers around ..."
docker stop salt-master salt-minion-1 salt-minion-2 salt-minion-3 salt-minion-4 salt-minion-5
echo "docker rm master/minions, if any stopped containers around ..."
docker rm salt-master salt-minion-1 salt-minion-2 salt-minion-3 salt-minion-4 salt-minion-5