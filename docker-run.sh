#!/bin/bash
echo "docker stop master/minions, if any running containers around ..."
docker stop salt-master salt-minion-1 salt-minion-2 salt-minion-3 salt-minion-4 salt-minion-5
echo "docker rm master/minions, if any stopped containers around ..."
docker rm salt-master salt-minion-1 salt-minion-2 salt-minion-3 salt-minion-4 salt-minion-5
echo "docker run master/minions and link them ..."
docker run -d -p 4505:4505 -p 4506:4506 --name=salt-master -h salt-master rcsa/salt-master:latest
docker run -d --link salt-master:salt-master --name=salt-minion-1 -h salt-minion-1 rcsa/salt-minion:latest
docker run -d --link salt-master:salt-master --name=salt-minion-2 -h salt-minion-2 rcsa/salt-minion:latest
docker run -d --link salt-master:salt-master --name=salt-minion-3 -h salt-minion-3 rcsa/salt-minion:latest
docker run -d --link salt-master:salt-master --name=salt-minion-4 -h salt-minion-4 rcsa/salt-minion:latest
docker run -d --link salt-master:salt-master --name=salt-minion-5 -h salt-minion-5 rcsa/salt-minion:latest
echo "waiting for minions to come up.."
sleep 10
echo "salt-key accept all minions"
docker exec salt-master salt-key -A -y
echo "waiting for master/minions to finalize handshake .."
sleep 10
echo "salt '*' test.ping all minions"
docker exec salt-master salt '*' test.ping
docker exec salt-master salt '*' cmd.run "uname -a"